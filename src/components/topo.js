import React, { Component } from 'react';
import {
  View,
  Image,
  StyleSheet
} from 'react-native';

//Topo do aplicativo que mostra a imagem
class Topo extends Component{
    render(){
      return(
        <View>
          <Image style={styles.imgTopo} source={require('../../img/jokenpo.png')}/>
        </View>
      );
    }
}

//Estilos usados aqui no topo
const styles = StyleSheet.create({
  imgTopo:{
    width:480,
    height: 250
  }
})

//Exportação da classe TOPO que será importada na classe principal APP3
export default Topo;