import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet
} from 'react-native';

//Classe icone que recebe da classe principal APP3, via props, escolha e jogador
class Icone extends Component{
    render(){
      //this.props.escolha
      //this.props.jogador
      
      // Apresentação dos resultados, parte visual
      if(this.props.escolha == 'Pedra'){
        return(
          <View style={styles.icone}>
            <Text style={styles.txtJogador}>{this.props.jogador}</Text>
            <Image source={require('../../img/pedra.png')} />
          </View>
        );
      }else if(this.props.escolha == 'Papel'){
        return(
          <View style={styles.icone}>
            <Text style={styles.txtJogador}>{this.props.jogador}</Text>
            <Image source={require('../../img/papel.png')} />
          </View>
        );
      } else if(this.props.escolha == 'Tesoura'){
        return(
          <View style={styles.icone}>
            <Text style={styles.txtJogador}>{this.props.jogador}</Text>
            <Image source={require('../../img/tesoura.png')} />
          </View>
        );
      } else {
        return false;
      }
    }
  }

// Estilos usados 
const styles = StyleSheet.create({
    txtResultado:{
      fontSize: 30,
      fontWeight: 'bold',
      color: 'red',
      height: 60
    },
    icone:{
      alignItems:'center',
      marginBottom: 20
    },
    txtJogador:{
      fontSize:25
    }
  });
//Exportando a classe que será chamada na classe principal APP3 
export default Icone;